﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './Core/Auth';

import { appRoutingModule } from './app.routing';
import { JwtInterceptor, ErrorInterceptor } from './Core/Auth';
import { AppComponent } from './app.component';
import { HomeComponent } from './Pages/home';
import { LoginComponent } from './Pages/login';
import { RegisterComponent } from './Pages/register';
import { AdminComponent } from './Pages/admin';
import { AlertComponent } from './_components';
import { CustomstringPipe } from './Core/Pipe/customstring.pipe';
import { ColorChangeDirective } from './Core/Directives/color-change.directive'
@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        appRoutingModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        AdminComponent,
        RegisterComponent,
        AlertComponent,
        CustomstringPipe ,
        ColorChangeDirective   ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { };