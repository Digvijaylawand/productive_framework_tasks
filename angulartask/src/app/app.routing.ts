﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './Pages/home';
import { AdminComponent } from './Pages/admin';
import { LoginComponent } from './Pages/login';
import { RegisterComponent } from './Pages/register';
import { AuthGuard } from './Core/Auth';
import { Role } from './Core/models';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.Admin] }
    },
    {
        path: 'login',
        component: LoginComponent
    },
    { 
        path: 'register', 
        component: RegisterComponent 
    },
    {
        path:'product',
        loadChildren:()=> import("./Pages/product/product.module").then(m=>m.ProductModule)
    },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);